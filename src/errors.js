let errors = {
    "server": {
        "internalserver":               500,

        "soon":                         501,
        "notimplemented":               501,
        
        "badgateway":                   502,
        
        "serviceunavalable":            503,
        
        "gatewaytime":                  504,
        "gatewaytimeout":               504,
        "gatewaytimedout":              504,
        
        "httpnotsupported":             505,
        "httpversionnotsupported":      505,
        
        "variantnegotiates":            506,
        "variantnegotiatestoo":         506,
        "variantalsonegotiates":        506,

        "insufficientstorage":          507, // (WebDAV)

        "loop":                         508, // (WebDAV)
        "infloop":                      508, // (WebDAV)
        "loopdetected":                 508, // (WebDAV)
        "infiniteloopdetected":         508, // (WebDAV)

        "notextended":                  510,
        "mustbeextended":               510,

        "networkauthrequired":          511,
        "netauthenticationrequired":    511
    },

    "client": {
        "badrequest":                   400,

        "unauthorized":                 401,

        "paymentrequired":              402, // to be used in the future

        "forbidden":                    403,

        "notfound":                     404,

        "methodnotallowed":             405,

        "notacceptable":                406,

        "proxyauth":                    407,
        "proxyauthrequired":            407,
        "proxyauthentication":          407,

        "timeout":                      408,
        "requesttimeout":               408,

        "conflict":                     409,

        "gone":                         410,
        "nolongerexists":               410,

        "lengthneeded":                 411,
        "headerlength":                 411,
        "lengthmissing":                411,
        "lengthrequired":               411,
        "headerlengthneeded":           411,
        "headerlengthmissing":          411,

        "precondition":                 412,
        "preconditionerror":            412,
        "preconditionfailed":           412,

        "payloadsize":                  413,
        "payloadlarge":                 413,
        "payloadtoobig":                413,
        "payloadtoolarge":              413,

        "uribig":                       414,
        "urilength":                    414,
        "uritoobig":                    414,
        "uritoolong":                   414,
        "urilengthtoobig":              414,

        "wrongmedia":                   415,
        "mediatypeerror":               415,
        "nomediasupport":               415,
        "unsupportedmedia":             415,

        "rangesatisfiederr":            416,
        "rangenotsatifiable":           416,

        "expectationerr":               417,
        "expectationfail":              417,
        "expectationerror":             417,
        "expectationfailed":            417,
        
        "tea":                          418,
        "pot":                          418,
        "teapot":                       418,
        "iamteapot":                    418,
        "iamateapot":                   418,

        "misredirectedrequest":         421, // NOTICE: Mozilla's HTTP code list does not state a sub-link for this request type. May not work well, idk

        "unabletoprocess":              422,
        "unprocessableentity":          422,

        "locked":                       423, // (WebDAV)

        "faileddependency":             424, // (WebDAV)

        "early":                        425,
        "2early":                       425,
        "tooearly":                     425,

        "upgrade":                      426,
        "upgradeprotocol":              426,
        "upgraderequired":              426,
        "protocolupgrade":              426,
        "protocolupgraderequired":      426,

        "preconditionmissing":          428,
        "preconditionrequired":         428,

        "ratelimit":                    429,
        "overloaded":                   429,
        "ratelimited":                  429,
        "toomanyrequests":              429,

        "headertoolarge":               431,
        "requestheadertoolarge":        431,

        "unavailablelegal":             451,
        "unavailablelegalreasons":      451,
        "unavailableforlegalreasons":   451,
        "unavailableduetolegalreasons": 451
    },
};

module.exports=errors