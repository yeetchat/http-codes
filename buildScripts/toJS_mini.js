/**
 * @author     0J3#9971
 * @name       success.js

 * @license    LGPL-3.0-or-later 
 * @copyright  Copyright (c) 2020 0J3. All Rights Reserved.
 * 
 * @generator  Generated using https://gitlab.com/yeetchat/http-codes/ (by building the module) 
 */
let conv = require("./convertMain")
let dir = "js_min"

function x(file,filename){
    return "/**\n * @author     0J3#9971\n * @name       "+ filename + "\n\n * @license    LGPL-3.0-or-later \n * @copyright  Copyright (c) 2020 0J3. All Rights Reserved.\n\n * @generator  Generated using https://gitlab.com/yeetchat/http-codes/ (by building the module) \n */\n\n// Define the array\nmodule.exports = "+JSON.stringify(file)+"\n"
}

conv(dir,".js",function(file,filename){
    file=require(file)

    file=x(file,filename)

    return file
})


conv()(function(f,r,array){
    return x(array,f)
},dir,"index.js")