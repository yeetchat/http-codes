require = function(m){
    console.log("REQUIRE "+m)
    return oldRequire(m)
}

const fs              = require("fs")
const stat            = fs.existsSync

const root_parent     = __dirname+"/../src"
const root            = root_parent+"/"

const extension       = ".js"

function convertFile(targetName,targetExtension,runFile) {
    if (typeof(targetName)==typeof(undefined)) {
        return function(run,target,file){
            const target_parent   = __dirname+"/../builds"
            target=target_parent+"/"+target

            if (!stat(target_parent)) {
                console.log("CREATE Target's Parent")
                fs.mkdirSync(target_parent)
            }
            
            if (stat(target)) {
                console.log("REMOVE Existing Target "+target)
                fs.rmdirSync(target, { recursive: true })
            }
    
            console.log("CREATE Target")
            fs.mkdirSync(target)

            let arr = {}
            
            fs.readdir(root, (err, files) => {
                if (err) {
                    throw err;
                }

                files.forEach(file => {
                    let x = require(root+file)

                    for (var b in x) {
                        let a = x[b]
                        for (var i in a) {
                            arr[i]=a[i]
                        };
                    };
                });
                
                fs.writeFile(target+"/"+file,run(file,root,arr),function(err){
                    if (err) throw err
                })
            });
        }
    } else {
        log=function(a){
            let spaces = "            "
            try {
                spaces=spaces.slice(targetName.length)
            }catch(e){
                oldlog("err here")
            }
            oldlog("Node @ Build :    #Convert-"+targetName+spaces+" :   "+a)
        }

        log("RUN Conversion for target "+targetName)

        const target_parent   = __dirname+"/../builds"

        const target          = target_parent+"/"+targetName

        if (!stat(root_parent)) {
            throw new Error("convertMain.js: Cannot find "+root_parent+" (CONVERSION ROOT PARENT DIRECTORY)")
        }
        if (!stat(root)) {
            throw new Error("convertMain.js: Cannot find "+root_parent+" (CONVERSION ROOT DIRECTORY)")
        }
        if (!stat(target_parent)) {
            log("CREATE Target's Parent")
            fs.mkdirSync(target_parent)
        }
        
        if (stat(target)) {
            log("REMOVE Existing Target "+target)
            fs.rmdirSync(target, { recursive: true })
        }

        log("CREATE Target")
        fs.mkdirSync(target)

        fs.readdir(root, (err, files) => {
            if (err) {
                throw err;
            }

            files.forEach(file => {
                const contents = runFile(root+file,file)
                log("START Conversion for file " + file);
                fs.writeFile(target+"/"+(file.replace(extension,targetExtension)),contents,function(err){
                    if (err) {
                        log("ERROR for Conversion for file "+file+": "+err)
                        log("END Conversion for file " + file);
                        throw err
                    }
                    log("END Conversion for file " + file);
                })
            });
        });
    }
}
module.exports=convertFile