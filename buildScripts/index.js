let oldRequire = require
global.oldRequire=oldRequire

let oldLog = console.log

console.log=function(e){
    let prefix = "#BUILD"
    let spaces = "                     "
    spaces=spaces.slice(prefix.length)
    oldLog("Node @ Build :    "+prefix+spaces+" :  ",e)
}
global.console.log=console.log
global.oldlog=oldLog

require = function(m){
    console.log("REQUIRE "+m)
    return oldRequire(m)
}
global.require=require

console.log("NOTICE Log messages might be out of order. I have no clue if this runs synchronously or asynchronously.")

// Remove old build
const stat = require("fs").existsSync
if (stat("../builds")) {
    console.log("REMOVE Existing Builds folder")
    fs.rmdirSync("../builds", { recursive: true })
}

function run() {
    // Minified Versions
    require("./toJSON_mini");
    require("./toJS_mini");

    // Pretty Printed Versions
    require("./toJSON_pretty");
    require("./toJS_pretty");
}

setTimeout(() => {
    run()
}, 1);
console.log("INFO if the files don\'t successfully generate, try re-running this.")
console.log("INFO if it still fails, delete the builds directory, then try again.")